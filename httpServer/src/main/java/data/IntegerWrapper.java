package data;

import lombok.Getter;

@Getter
public class IntegerWrapper {
    private final int integer;

    public IntegerWrapper(int integer) {
        this.integer = integer;
    }

    public static IntegerWrapper randomize() {
        return new IntegerWrapper((int) (Math.random() * 100 + 1));
    }
}
