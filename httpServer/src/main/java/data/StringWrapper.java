package data;

import lombok.Getter;

@Getter
public class StringWrapper {
    private final String str1;
    private final String str2;

    public StringWrapper(String str1, String str2) {
        this.str1 = str1;
        this.str2 = str2;
    }

    public static StringWrapper randomize() {
        return new StringWrapper(randomStr(), randomStr());
    }

    private static String randomStr() {
        final char[] charSet = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
        int size = (int) ((Math.random() * (20 - 5 + 1)) + 5);
        char[] strCh = new char[size];

        for (int i = 0; i < size; ++i) {
            strCh[i] = charSet[(int) ((Math.random() * charSet.length))];
        }
        return String.valueOf(strCh);
    }
}
