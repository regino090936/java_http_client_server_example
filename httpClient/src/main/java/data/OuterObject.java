package data;

import lombok.Getter;
import java.util.ArrayList;
import java.util.List;

@Getter
public class OuterObject {
    private final StringWrapper str;
    private final StringWrapperWrapper strWrapper;
    private final List<IntegerWrapper> integers;

    public List<IntegerWrapper> getIntegers() {
        return new ArrayList<>(integers);
    }

    public OuterObject(StringWrapper str, StringWrapperWrapper strWrapper, List<IntegerWrapper> integers) {
        this.str = str;
        this.strWrapper = strWrapper;
        this.integers = integers;
    }

    public static OuterObject randomize() {
        List<IntegerWrapper> integers = new ArrayList<>(5);
        for (int i = 0, size = (int) ((Math.random() * (5 - 2)) + 2); i < size; ++i) {
            integers.add(IntegerWrapper.randomize());
        }

        OuterObject obj = new OuterObject(
                StringWrapper.randomize(),
                StringWrapperWrapper.randomize(),
                integers
        );

        return obj;
    }
}

