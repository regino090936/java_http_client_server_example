package data;

import lombok.Getter;

@Getter
public class StringWrapperWrapper {
    private final StringWrapper str1;
    private final StringWrapper str2;

    public StringWrapperWrapper(StringWrapper strW1, StringWrapper strW2) {
        this.str1 = strW1;
        this.str2 = strW2;
    }

    public static StringWrapperWrapper randomize() {
        return new StringWrapperWrapper(StringWrapper.randomize(), StringWrapper.randomize());
    }
}
