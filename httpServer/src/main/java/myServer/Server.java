package myServer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

public class Server {
    private static final Logger log = LoggerFactory.getLogger(Server.class);

    public static void main(String[] args) throws Exception {
        new File("data/response").mkdirs();
        new File("data/logs").mkdir();

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        ServerThread serverThread = new ServerThread();

        boolean isServerWork = false;
        boolean isProgramWork = true;
        String select;
        System.out.println("Enter: '1' to start/stop server, '2' to exit program");
        while (isProgramWork) {
            select = reader.readLine();
            switch (select) {
                case "1":
                    if (isServerWork) {
                        serverThread.stopServer();
                        log.info("STOP SERVER");
                    } else {
                        serverThread.start();
                        log.info("START SERVER");
                    }
                    isServerWork = !isServerWork;
                    break;
                case "2":
                    if (isServerWork) {
                        serverThread.stopServer();
                        log.info("STOP SERVER");
                    }
                    isProgramWork = false;
                    log.info("EXIT PROGRAM");
                    break;
                default:
                    break;
            }
        }
    }
}
