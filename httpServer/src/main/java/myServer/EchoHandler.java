package myServer;

import com.squareup.moshi.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import data.IntegerWrapper;
import data.OuterObject;
import data.StringWrapper;
import data.StringWrapperWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class EchoHandler implements HttpHandler {
    private static int applicationId;
    private static int threadId = 0;

    static {
        try {
            File sessionFile = new File("data/session_id.txt");
            if (sessionFile.exists()) {
                BufferedReader fr = new BufferedReader(new FileReader(sessionFile));
                applicationId = Integer.parseInt(fr.readLine());
                fr.close();
            } else {
                applicationId = 1;
            }
            FileWriter fw = new FileWriter(sessionFile);
            fw.write(Integer.toString(applicationId + 1));
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int thisThreadId;

    public EchoHandler() {
        this.thisThreadId = ++threadId;
    }

    private static final Logger log = LoggerFactory.getLogger(EchoHandler.class);

    @Override
    public void handle(HttpExchange exchange)  throws IOException {
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<OuterObject> jsonAdapter = moshi.adapter(OuterObject.class);

        InputStream is = exchange.getRequestBody();
        String jsonStr = new String(is.readAllBytes(), StandardCharsets.UTF_8);
        log.info("receive: [{}\n  {}]", jsonStr.substring(0, jsonStr.length() / 2), jsonStr.substring(jsonStr.length() / 2));
        OuterObject obj = jsonAdapter.fromJson(jsonStr);

        obj = modifyObject(obj);
        jsonStr = jsonAdapter.toJson(obj);
        byte[] bytes = jsonStr.getBytes();
        log.info("send: [{}\n  {}]", jsonStr.substring(0, jsonStr.length() / 2), jsonStr.substring(jsonStr.length() / 2));

        exchange.sendResponseHeaders(200, bytes.length);

        OutputStream os = exchange.getResponseBody();
        os.write(bytes);
        os.close();

        FileOutputStream fw = new FileOutputStream(String.format("data/response/server_response_%d_%d.txt", applicationId, thisThreadId), true);
        fw.write(bytes);
        fw.write('\n');
        fw.close();
    }

    private OuterObject modifyObject(OuterObject obj) {
        List<IntegerWrapper> list = obj.getIntegers();
        for (IntegerWrapper integerWrapper : list)
            integerWrapper = new IntegerWrapper(integerWrapper.getInteger() * 2);

        OuterObject newObj = new OuterObject(
                new StringWrapper(
                        new StringBuffer(obj.getStr().getStr1()).reverse().toString(),
                        new StringBuffer(obj.getStr().getStr2()).reverse().toString()
                ),
                new StringWrapperWrapper(
                        new StringWrapper(
                                new StringBuffer(obj.getStrWrapper().getStr1().getStr1()).reverse().toString(),
                                new StringBuffer(obj.getStrWrapper().getStr1().getStr2()).reverse().toString()
                        ),
                        new StringWrapper(
                                new StringBuffer(obj.getStrWrapper().getStr2().getStr1()).reverse().toString(),
                                new StringBuffer(obj.getStrWrapper().getStr2().getStr2()).reverse().toString()
                        )
                ),
                list
        );
        return newObj;
    }
}
