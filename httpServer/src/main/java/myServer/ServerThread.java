package myServer;

import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;

public class ServerThread{
    private HttpServer server;
    private static final Logger log = LoggerFactory.getLogger(ServerThread.class);

    public void start() {
        try {
            server = HttpServer.create();
            server.bind(new InetSocketAddress(8765), 0);
            HttpContext context = server.createContext("/random_json", new EchoHandler());
            server.setExecutor(null);
            server.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void stopServer() {
        server.stop(0);
    }
}
