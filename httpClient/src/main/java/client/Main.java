package client;

import com.google.gson.*;
import data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.http.*;
import java.net.*;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;


public class Main {

    private static int applicationId;
    private static int threadId = 0;

    static {
        try {
            new File("data/response").mkdirs();
            new File("data/logs").mkdir();

            File sessionFile = new File("data/session_id.txt");
            if (sessionFile.exists()) {
                BufferedReader fr = new BufferedReader(new FileReader(sessionFile));
                applicationId = Integer.parseInt(fr.readLine());
                fr.close();
            } else {
                applicationId = 1;
            }
            FileWriter fw = new FileWriter(sessionFile);
            fw.write(Integer.toString(applicationId + 1));
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isInfinity;
    private static int amount = 0;
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        HttpClient client = HttpClient.newHttpClient();
        Gson gson = new Gson();
        isInfinity = false;
        amount = Integer.parseInt(args[0]);
        try {
            log.debug("START SENDING");
            while (isInfinity || (amount-- > 0)) {
                OuterObject obj = OuterObject.randomize();
                String jsonStr = gson.toJson(obj);

                HttpRequest request = HttpRequest.newBuilder()
                        .header("accept", "application/json")
                        .POST(HttpRequest.BodyPublishers.ofString(jsonStr))
                        .uri(URI.create("http://localhost:8765/random_json"))
                        .build();

                HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

                jsonStr = gson.toJson(returnToOriginalState(gson.fromJson(response.body(), OuterObject.class)));

                FileWriter fw = new FileWriter(String.format("data/response/server_response_%d.txt", applicationId), true);
                fw.write(jsonStr);
                fw.append('\n');
                fw.close();
                log.info("[{}\n  {}]", jsonStr.substring(0, jsonStr.length() / 2), jsonStr.substring(jsonStr.length() / 2));
                Thread.sleep(2000);
            }
            log.debug("STOP SENDING");
        }catch (ConnectException e) {
            log.warn("cannot connect to server");
        }catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    static OuterObject returnToOriginalState(OuterObject obj) {
        List<IntegerWrapper> list = obj.getIntegers().stream()
                .map(integerWrapper -> new IntegerWrapper(integerWrapper.getInteger() / 2))
                .collect(Collectors.toList());

        OuterObject newObj = new OuterObject(
                new StringWrapper(
                        new StringBuffer(obj.getStr().getStr1()).reverse().toString(),
                        new StringBuffer(obj.getStr().getStr2()).reverse().toString()
                ),
                new StringWrapperWrapper(
                        new StringWrapper(
                                new StringBuffer(obj.getStrWrapper().getStr1().getStr1()).reverse().toString(),
                                new StringBuffer(obj.getStrWrapper().getStr1().getStr2()).reverse().toString()
                        ),
                        new StringWrapper(
                                new StringBuffer(obj.getStrWrapper().getStr2().getStr1()).reverse().toString(),
                                new StringBuffer(obj.getStrWrapper().getStr2().getStr2()).reverse().toString()
                        )
                ),
                list
        );
        return newObj;
    }

}
